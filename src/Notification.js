export default {
    template: `<div>{{notification}}</div>`,
    props: ['message'],
    computed: {
        notification(value){
            return this.message.toUpperCase();
        }
    }
}